import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape{

    private final TwoDPoint[] vertices = new TwoDPoint[4];

    public Quadrilateral(double... vertices) { 
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices) {
        int n = 0;
        for (TwoDPoint p : vertices) 
        {
            this.vertices[n] = p;
            n++;
        }
        if (!isMember(vertices))
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getCanonicalName()));
    }

    /**
     * Given a list of four points, adds them as the four vertices of this quadrilateral in the order provided in the
     * list. This is expected to be a counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points) throws IllegalStateException{
        if(points.size() != 4) throw new IllegalStateException("The number of vertices provided as input is not equal to four.");
        this.vertices[0] = (TwoDPoint) points.get(0);
        this.vertices[1] = (TwoDPoint) points.get(1);
        this.vertices[2] = (TwoDPoint) points.get(2);
        this.vertices[3] = (TwoDPoint) points.get(3);
    }

    @Override
    public List<TwoDPoint> getPosition() {
        return Arrays.asList(vertices);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the setter {@link Quadrilateral#setPosition(List)}
     *         expected the corners to be provided in a counterclockwise order, the side lengths are expected to be in
     *         that same order.
     */
    protected double[] getSideLengths() {
        double[] result = new double[4];
        result[0] = Math.sqrt(Math.pow(this.vertices[0].coordinates()[0] - this.vertices[1].coordinates()[0],2) + Math.pow(this.vertices[0].coordinates()[1] - this.vertices[1].coordinates()[1],2)); // sqrt((x1-x2)^2 + (y1-y2)^2)
        result[1] = Math.sqrt(Math.pow(this.vertices[1].coordinates()[0] - this.vertices[2].coordinates()[0],2) + Math.pow(this.vertices[1].coordinates()[1] - this.vertices[2].coordinates()[1],2));
        result[2] = Math.sqrt(Math.pow(this.vertices[2].coordinates()[0] - this.vertices[3].coordinates()[0],2) + Math.pow(this.vertices[2].coordinates()[1] - this.vertices[3].coordinates()[1],2));
        result[3] = Math.sqrt(Math.pow(this.vertices[3].coordinates()[0] - this.vertices[0].coordinates()[0],2) + Math.pow(this.vertices[3].coordinates()[1] - this.vertices[0].coordinates()[1],2));
        return result; // TODO
    }

    @Override
    public int numSides() { return 4; }

    @Override
    public boolean isMember(List<? extends Point> vertices) { return vertices.size() == 4; }
    
    @Override
    public double x_location(){
        double xs = this.vertices[1].coordinates()[0];
        return xs;
    }
}
