
import java.util.Collections;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author shuzhou
 */
public class Sphere implements Positionable,ThreeDShape{
    private ThreeDPoint center;
    private double radius;
    public Sphere(double x, double y, double z, double r){
        this.center = new ThreeDPoint(x,y,z);
        this.radius = r;
    }
    
    @Override
    public Point center(){
        return this.center;
    }
    
    @Override
    public void setPosition(List<? extends Point> points) throws IllegalArgumentException{
        if(!(points.get(0) instanceof ThreeDPoint)) { throw new IllegalArgumentException("The input does not consist of ThreeDPoint instances."); }
        this.center = (ThreeDPoint) (points.get(0));
    }
    
    @Override
    public List<? extends Point> getPosition() {
        return Collections.singletonList(center);
    }
    
    public void setRadius(double r) { this.radius = r; }

    public double getRadius()       { return radius; }

    public int numSides() {
        return 0; 
    }

    public boolean isMember(List<? extends Point> centers) {
        return centers.size() == 1 && radius > 0;
    }
    
    @Override
    public double volume(){
        return 4 * Math.PI * Math.pow(this.radius, 3) / 3;
    }
    
    @Override
    public int compareTo(ThreeDShape c){
        if(this.volume() == c.volume()) return 0;
        if(this.volume() < c.volume()) return -1;
        return 1;
    }

    @Override
    public double surfaceArea() {
        return 4 * Math.PI * Math.pow(radius, 2);
    }
    
    public static Sphere random(){
        return new Sphere(Math.random()*200-100, Math.random()*200-100, Math.random()*200-100, Math.random() * 50);
    }
}
