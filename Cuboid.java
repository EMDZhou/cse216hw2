import java.util.ArrayList;
import java.util.List;

// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().
public class Cuboid implements ThreeDShape {

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
    }

    @Override
    public double volume() {
        // (v0 - v1) * (v0 - v3) * (v5 - v0)
        double x = this.vertices[0].coordinates()[0] - this.vertices[1].coordinates()[0]; //v0.x - v1.x
        double y = this.vertices[5].coordinates()[1] - this.vertices[0].coordinates()[1]; //v5.y - v0.y
        double z = this.vertices[0].coordinates()[2] - this.vertices[3].coordinates()[2]; //v0.z - v3.z
        return x*y*z; // TODO
    }

    @Override
    public ThreeDPoint center() {
        double x_offset = (this.vertices[0].coordinates()[0] - this.vertices[1].coordinates()[0]) / 2;//x center, (v0.x - v1.x) / 2
        double y_offset = (this.vertices[5].coordinates()[1] - this.vertices[0].coordinates()[1]) / 2;//y center, (v5.y - v0.y) / 2
        double z_offset = (this.vertices[0].coordinates()[2] - this.vertices[3].coordinates()[2]) / 2;//z center, (v0.z - v3.z) / 2
        return new ThreeDPoint(this.vertices[0].coordinates()[0] - x_offset, this.vertices[0].coordinates()[1] - y_offset, this.vertices[0].coordinates()[2] - z_offset); // TODO
    }
    
    @Override
    public int compareTo(ThreeDShape c){
        if(this.volume() == c.volume()) return 0;
        if(this.volume() < c.volume()) return -1;
        return 1;
    }

    @Override
    public double surfaceArea() {
        //(x*y + y*z + x*z) * 2
        double x = this.vertices[0].coordinates()[0] - this.vertices[1].coordinates()[0]; //v0.x - v1.x
        double y = this.vertices[5].coordinates()[1] - this.vertices[0].coordinates()[1]; //v5.y - v0.y
        double z = this.vertices[0].coordinates()[2] - this.vertices[3].coordinates()[2]; //v0.z - v3.z
        return (x*y + y*z + x*z)*2;
    }
    
    public static Cuboid random(){
        List<ThreeDPoint> vertices = new ArrayList<ThreeDPoint>();
        // generate the origin
        vertices.add(new ThreeDPoint(Math.random()*200-100, Math.random()*200-100, Math.random()*200-100));
        // generate v1, v2, v3
        vertices.add(new ThreeDPoint(Math.random()*200-100 + vertices.get(0).coordinates()[0],
                     vertices.get(0).coordinates()[1],
                     vertices.get(0).coordinates()[2])); //y and z the same as v0, x random in [-100,100]
        vertices.add(new ThreeDPoint(vertices.get(1).coordinates()[0],
                     vertices.get(1).coordinates()[1],
                     Math.random()*200-100 + vertices.get(1).coordinates()[2])); //x and y the same as v1, z random in [-100,100]
        vertices.add(new ThreeDPoint(vertices.get(0).coordinates()[0],
                     vertices.get(0).coordinates()[1],
                     vertices.get(2).coordinates()[2])); // v0's x, v0's y, v2's z
        // the rest only changes in a specific value of y
        double y_offset = Math.random()*200-100;
        vertices.add(new ThreeDPoint(vertices.get(3).coordinates()[0],
                     vertices.get(3).coordinates()[1] + y_offset,
                     vertices.get(3).coordinates()[2])); // v4 correspond to v3
        vertices.add(new ThreeDPoint(vertices.get(0).coordinates()[0],
                     vertices.get(0).coordinates()[1] + y_offset,
                     vertices.get(0).coordinates()[2])); // v5 correspond to v0
        vertices.add(new ThreeDPoint(vertices.get(1).coordinates()[0],
                     vertices.get(1).coordinates()[1] + y_offset,
                     vertices.get(1).coordinates()[2])); // v6 correspond to v1
        vertices.add(new ThreeDPoint(vertices.get(2).coordinates()[0],
                     vertices.get(2).coordinates()[1] + y_offset,
                     vertices.get(2).coordinates()[2])); // v7 correspond to v2
        return new Cuboid(vertices);
    }
}
