import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {

    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() {
        double x = (this.getPosition().get(0).coordinates()[0] + this.getPosition().get(2).coordinates()[0]) / 2;
        double y = (this.getPosition().get(0).coordinates()[1] + this.getPosition().get(2).coordinates()[1]) / 2;
        return new TwoDPoint(x,y); // TODO
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        return vertices.size() == 4 
                && (this.getPosition().get(0).coordinates()[1] == this.getPosition().get(1).coordinates()[1]) 
                && (this.getPosition().get(0).coordinates()[0] == this.getPosition().get(3).coordinates()[0])
                && (this.getPosition().get(2).coordinates()[1] == this.getPosition().get(3).coordinates()[1])
                && (this.getPosition().get(2).coordinates()[0] == this.getPosition().get(1).coordinates()[0]);// TODO
    }

    @Override
    public double area() {
        return this.getSideLengths()[0] * this.getSideLengths()[1]; // TODO
    }
    
    public Rectangle(List<TwoDPoint> vertices){
        super(vertices);
    }
}
