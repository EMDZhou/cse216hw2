import java.util.*;

public class Ordering {

    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override public int compare(TwoDShape o1, TwoDShape o2) {
            if(o1.x_location()== o2.x_location()) return 0;
            if(o1.x_location() < o2.x_location()) return -1;
            return 1; // TODO
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            if(o1.area() == o2.area()) return 0; 
            if(o1.area() < o2.area()) return -1;
            return 1;// TODO
        }
    }

    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override public int compare(ThreeDShape o1, ThreeDShape o2) {
            if(o1.surfaceArea() == o2.surfaceArea()) return 0; 
            if(o1.surfaceArea() < o2.surfaceArea()) return -1;
            return 1;// TODO
        }
    }

    // TODO: there's a lot wrong with this method. correct it so that it can work properly with generics.
    static <A, B> void copy(Collection<A> source, Collection<B> destination) {
        for(Object s : source){
            destination.add((B) s);
        }
    }

    public static void main(String[] args) {
        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape>        threedshapes    = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. */
        List<TwoDPoint> points = new ArrayList<>();
        points.add(new TwoDPoint(2,3));
        points.add(new TwoDPoint(1,3));
        points.add(new TwoDPoint(1,2));
        points.add(new TwoDPoint(2,2));
        List<TwoDPoint> points1 = new ArrayList<>();
        points1.add(new TwoDPoint(5,3));
        points1.add(new TwoDPoint(1,3));
        points1.add(new TwoDPoint(1,2));
        points1.add(new TwoDPoint(5,2));
        List<TwoDPoint> points2 = new ArrayList<>();
        points2.add(new TwoDPoint(8,3));
        points2.add(new TwoDPoint(1,3));
        points2.add(new TwoDPoint(1,2));
        points2.add(new TwoDPoint(8,2));
        
        
        List<TwoDPoint> points3 = new ArrayList<>();
        points3.add(new TwoDPoint(2,3));
        points3.add(new TwoDPoint(1,3));
        points3.add(new TwoDPoint(1,2));
        points3.add(new TwoDPoint(2,2));

        List<TwoDPoint> points4 = new ArrayList<>();
        points4.add(new TwoDPoint(3,4));
        points4.add(new TwoDPoint(1,4));
        points4.add(new TwoDPoint(1,2));
        points4.add(new TwoDPoint(3,2));

        List<TwoDPoint> points5 = new ArrayList<>();
        points5.add(new TwoDPoint(5,6));
        points5.add(new TwoDPoint(1,6));
        points5.add(new TwoDPoint(1,2));
        points5.add(new TwoDPoint(5,2));
        
        
        symmetricshapes.add(new Rectangle(points));
        symmetricshapes.add(new Rectangle(points1));
        symmetricshapes.add(new Rectangle(points2));
        
        symmetricshapes.add(new Square(points3));
        symmetricshapes.add(new Square(points4));
        symmetricshapes.add(new Square(points5));
        
        symmetricshapes.add(new Circle(1,2,3));
        symmetricshapes.add(new Circle(3,2,3));
        symmetricshapes.add(new Circle(2,2,3));

        copy(symmetricshapes, shapes); // note-1 //
        shapes.add(new Quadrilateral(points));
        

        // sorting 2d shapes according to various criteria
        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        symmetricshapes.sort(new AreaComparator());

        // sorting 3d shapes according to various criteria
        Collections.sort(threedshapes);
        threedshapes.sort(new SurfaceAreaComparator());

        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. */

        
        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //
        
    }
}
